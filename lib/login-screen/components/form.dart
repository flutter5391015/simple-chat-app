import 'dart:io';

import 'package:chat_app/components/toast/error_toast.dart';
import 'package:chat_app/components/toast/success_toast.dart';
import 'package:chat_app/login-screen/components/atoms/user_image_picker.dart';
import 'package:chat_app/login-screen/components/atoms/input.dart';
import 'package:chat_app/theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_indicator/loading_indicator.dart';
import "package:firebase_storage/firebase_storage.dart";

final _firebase = FirebaseAuth.instance;
final _toast = FToast();

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late FocusNode _focusEmailNode;
  late FocusNode _focusNameNode;
  late FocusNode _focusPassNode;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _hasAccount = true;
  bool _isLoading = false;
  late TextEditingController _emailController;
  late TextEditingController _nameController;
  late TextEditingController _passController;
  late File? _selectedImage;

  @override
  void initState() {
    super.initState();
    _toast.init(context);
    _focusEmailNode = FocusNode();
    _focusPassNode = FocusNode();
    _focusNameNode = FocusNode();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _passController = TextEditingController();
  }

  @override
  void dispose() {
    _focusEmailNode.dispose();
    _focusPassNode.dispose();
    _focusNameNode.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _passController.dispose();
    super.dispose();
  }

  void _requestFocus(FocusNode focusNode) {
    setState(() {
      FocusScope.of(context).requestFocus(focusNode);
    });
  }

  void _imageSelected(File image) {
    setState(() {
      _selectedImage = image;
    });
  }

  void _submitHandler() async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) return;
    if (!_hasAccount && _selectedImage == null) {
      _toast.showToast(
        child: const ToastError(
          message: 'Please select image first',
        ),
        gravity: ToastGravity.TOP,
      );
      return;
    }

    _formKey.currentState!.save();

    try {
      setState(() {
        _isLoading = true;
      });

      if (_hasAccount) {
        final userCredential = await _firebase.signInWithEmailAndPassword(
            email: _emailController.text, password: _passController.text);
        print(userCredential);
      } else {
        final userCredential = await _firebase.createUserWithEmailAndPassword(
            email: _emailController.text, password: _passController.text);
        final avatarStorageRef = FirebaseStorage.instance
            .ref()
            .child('avatar')
            .child('${userCredential.user!.uid}.jpg');

        await avatarStorageRef.putFile(_selectedImage!);
        final avatarUrl = await avatarStorageRef.getDownloadURL();

        await FirebaseFirestore.instance
            .collection('users')
            .doc(userCredential.user!.uid)
            .set({
          'name': _nameController.text,
          'email': _emailController.text,
          'image_url': avatarUrl,
        });

        setState(() {
          _hasAccount = true;
          _isLoading = false;
        });

        _toast.showToast(
          child: const ToastSuccess(
            message: 'Success creating new user',
          ),
          gravity: ToastGravity.TOP,
        );
      }
    } on FirebaseAuthException catch (error) {
      _toast.showToast(
        child: ToastError(
          message: error.message ?? 'Error while creating new user',
        ),
        gravity: ToastGravity.TOP,
      );

      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Stack(
        children: [
          Positioned(
            child: SingleChildScrollView(
              child: ListBody(children: [
                SizedBox(
                  height: 10.h,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Wrap(
                    children: [
                      Text(
                        'Welcome to',
                        style:
                            Theme.of(context).textTheme.labelMedium!.copyWith(
                                  fontWeight: FontWeight.w700,
                                ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      Text(
                        'FlutterChat',
                        style:
                            Theme.of(context).textTheme.labelMedium!.copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: MyAppColor.RED,
                                ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                if (!_hasAccount)
                  UserImagePicker(
                    onImageSelected: (selectedImage) =>
                        _imageSelected(selectedImage),
                  ),
                if (!_hasAccount)
                  SizedBox(
                    height: 10.h,
                  ),
                InputForm(
                  controller: _emailController,
                  inputFocusNode: _focusEmailNode,
                  title: 'Email',
                  autocorrect: false,
                  hintText: 'Please input your email',
                  onTap: () => _requestFocus(_focusEmailNode),
                  textInputType: TextInputType.emailAddress,
                  paddingBottom: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 1000.h,
                  ),
                  validator: (value) {
                    if (value == null ||
                        value.trim().isEmpty ||
                        !value.contains('@')) {
                      return "Please enter a  valid email address";
                    }
                    return null;
                  },
                ),
                if (!_hasAccount)
                  SizedBox(
                    height: 15.h,
                  ),
                if (!_hasAccount)
                  InputForm(
                    controller: _nameController,
                    inputFocusNode: _focusNameNode,
                    title: 'Name',
                    autocorrect: false,
                    hintText: 'Please input your name',
                    onTap: () => _requestFocus(_focusNameNode),
                    textInputType: TextInputType.text,
                    paddingBottom: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom + 1000.h,
                    ),
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return "Please enter a name";
                      }
                      return null;
                    },
                  ),
                SizedBox(
                  height: 15.h,
                ),
                InputForm(
                  controller: _passController,
                  inputFocusNode: _focusPassNode,
                  title: 'Password',
                  hintText: 'Please input your password',
                  autocorrect: false,
                  obscureText: true,
                  onTap: () => _requestFocus(_focusPassNode),
                  paddingBottom: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom + 1000.h,
                  ),
                  validator: (value) {
                    if (value == null || value.trim().length < 6) {
                      return "Password must be at least 6 characters long.";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 100.h,
                ),
              ]),
              //mainAxisAlignment: MainAxisAlignment.start,
              //crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
          Positioned(
            bottom: 0.h,
            left: 0,
            right: 0,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: SizedBox(
                child: Column(
                  children: [
                    TextButton(
                      onPressed: () => setState(() {
                        _hasAccount = !_hasAccount;
                      }),
                      child: Text(
                        _hasAccount
                            ? 'Create an account'
                            : 'I already have an account',
                        style: Theme.of(context).textTheme.labelSmall!.copyWith(
                              color: MyAppColor.BLACK,
                            ),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: ElevatedButton(
                        onPressed: _isLoading ? null : _submitHandler,
                        child: _isLoading
                            ? const SizedBox(
                                height: 20,
                                width: 20,
                                child: LoadingIndicator(
                                  indicatorType: Indicator.ballSpinFadeLoader,
                                  colors: [MyAppColor.WHITE],
                                  strokeWidth: 2,
                                  backgroundColor: MyAppColor.RED,
                                ),
                              )
                            : Text(
                                _hasAccount ? 'Login' : 'Sign up',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelMedium!
                                    .copyWith(
                                      color: MyAppColor.WHITE,
                                    ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
