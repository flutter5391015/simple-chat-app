import 'dart:io';

import 'package:chat_app/theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

class UserImagePicker extends StatefulWidget {
  final Function(File selectedImage) onImageSelected;
  const UserImagePicker({super.key, required this.onImageSelected});

  @override
  State<UserImagePicker> createState() => _UserImagePickerState();
}

class _UserImagePickerState extends State<UserImagePicker> {
  File? _pickedImageFile;

  void _pickImageHandler() async {
    final pickImage = await ImagePicker().pickImage(
        source: ImageSource.camera, imageQuality: 50, maxWidth: 150.w);

    if (pickImage == null) return;

    setState(() {
      _pickedImageFile = File(pickImage.path);
    });
    widget.onImageSelected(_pickedImageFile!);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _pickImageHandler,
      child: Center(
        child: DottedBorder(
          color: MyAppColor.PLACEHOLDER,
          strokeWidth: 1,
          borderType: BorderType.Circle,
          dashPattern: const [8, 4],
          child: Container(
            height: 100.w,
            width: 100.w,
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.r),
            ),
            child: CircleAvatar(
              radius: 20.r,
              backgroundColor: MyAppColor.GREY,
              foregroundImage: _pickedImageFile != null
                  ? FileImage(_pickedImageFile!)
                  : null,
              child: _pickedImageFile == null
                  ? Text(
                      'Add Photo',
                      style: Theme.of(context).textTheme.labelSmall!,
                    )
                  : null,
            ),
          ),
        ),
      ),
    );
  }
}
