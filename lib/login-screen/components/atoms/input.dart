import 'package:chat_app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InputForm extends StatelessWidget {
  final FocusNode inputFocusNode;
  final String title;
  final TextEditingController? controller;
  final String? hintText;
  final TextInputType? textInputType;
  final TextCapitalization? textCapitalization;
  final VoidCallback? onTap;
  final EdgeInsets? paddingBottom;
  final bool? obscureText;
  final bool? autocorrect;
  final String? Function(String?)? validator;
  final Function(String?)? onSaved;

  const InputForm(
      {super.key,
      required this.controller,
      required this.inputFocusNode,
      required this.title,
      this.textInputType,
      this.textCapitalization,
      this.hintText,
      this.onTap,
      this.paddingBottom,
      this.obscureText,
      this.autocorrect,
      this.validator,
      this.onSaved});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.labelSmall!.copyWith(
                fontWeight: FontWeight.w700,
                fontSize: 14.sp,
              ),
        ),
        SizedBox(
          height: 5.h,
        ),
        TextFormField(
          controller: controller,
          autocorrect: autocorrect ?? false,
          obscureText: obscureText ?? false,
          onSaved: onSaved,
          keyboardType: textInputType ?? TextInputType.text,
          textCapitalization: textCapitalization ?? TextCapitalization.none,
          focusNode: inputFocusNode,
          onTap: onTap,
          validator: validator,
          onTapOutside: (event) => FocusScope.of(context).unfocus(),
          scrollPadding: paddingBottom ?? EdgeInsets.zero,
          cursorColor: MyAppColor.RED,
          decoration: InputDecoration(
            hintText: hintText ?? '',
            hintStyle: TextStyle(
              fontSize: 14.sp,
              color: MyAppColor.PLACEHOLDER,
            ),
            contentPadding:
                EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.r),
              borderSide: const BorderSide(
                color: MyAppColor.BORDER_TEXT_INPUT,
                width: 1,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.r),
              borderSide: const BorderSide(
                color: MyAppColor.RED,
                width: 1,
              ),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.r),
              borderSide: const BorderSide(
                color: MyAppColor.RED,
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.r),
              borderSide: const BorderSide(
                color: MyAppColor.BLACK,
                width: 1,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
