import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class MyAppColor {
  static const Color GREY = Color(0xFFF1F2F6);
  static const Color GREEN = Color(0xFF1CBA3F);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color BLACK = Color(0xFF1E272E);
  static const Color GREY_DARK = Color(0xFF747D8C);
  static const Color YELLOW = Color(0xFFF7B731);
  static const Color RED = Color(0xFFEC2028);
  static const Color RED700 = Color(0xFFBA2A1C);
  static const Color GREY_LIGHT = Color(0xFFE4E5EA);
  static const Color BORDER_TEXT_INPUT = Color(0xFFCED6E0);
  static const Color PLACEHOLDER = Color(0xFFA4B0BE);
  static const LinearGradient LINER_RED = LinearGradient(
    colors: [
      Color(0xFFE52D27),
      Color(0xFFB31217),
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}

class AppTheme {
  static ThemeData lightTheme() {
    return ThemeData(
      colorScheme: ColorScheme.fromSeed(
        seedColor: MyAppColor.WHITE,
      ),
      textTheme: GoogleFonts.openSansTextTheme().copyWith(
        labelSmall: GoogleFonts.plusJakartaSans(
          fontWeight: FontWeight.normal,
          color: MyAppColor.BLACK,
          fontSize: 14.sp,
        ),
        labelMedium: GoogleFonts.plusJakartaSans(
          fontWeight: FontWeight.w500,
          color: MyAppColor.BLACK,
          fontSize: 16.sp,
        ),
        labelLarge: GoogleFonts.plusJakartaSans(
          fontWeight: FontWeight.w700,
          color: MyAppColor.BLACK,
          fontSize: 24.sp,
        ),
      ),
      textButtonTheme: const TextButtonThemeData(
        style: ButtonStyle(
          enableFeedback: false,
          overlayColor: MaterialStatePropertyAll(
            Colors.transparent,
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
            MyAppColor.RED,
          ),
          overlayColor: MaterialStatePropertyAll(
            MyAppColor.RED.withOpacity(
              0.9,
            ),
          ),
        ),
      ),
      useMaterial3: true,
    );
  }
}
