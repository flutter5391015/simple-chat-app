import 'package:chat_app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ToastError extends StatelessWidget {
  final String message;
  const ToastError({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.r),
        color: MyAppColor.RED700,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            Icons.close_outlined,
            color: MyAppColor.WHITE,
          ),
          SizedBox(
            width: 10.w,
          ),
          Flexible(
            child: Text(
              message,
              style: Theme.of(context)
                  .textTheme
                  .labelMedium!
                  .copyWith(color: MyAppColor.WHITE),
            ),
          ),
        ],
      ),
    );
  }
}
