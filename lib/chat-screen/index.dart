import 'package:chat_app/chat-screen/components/atoms/list.dart';
import 'package:chat_app/chat-screen/components/atoms/new_message.dart';
import 'package:chat_app/theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

final _firebase = FirebaseAuth.instance;

class ChatScreen extends StatelessWidget {
  const ChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('FlutterChat'),
        centerTitle: false,
        backgroundColor: MyAppColor.RED,
        actions: [
          IconButton(
            onPressed: () => _firebase.signOut(),
            enableFeedback: false,
            highlightColor: Colors.red.withOpacity(0.8),
            icon: const Icon(
              Icons.logout_outlined,
              color: MyAppColor.WHITE,
            ),
          ),
        ],
      ),
      body: const Column(
        children: [
          Expanded(
            child: ListMessage(),
          ),
          NewMessage(),
        ],
      ),
    );
  }
}
