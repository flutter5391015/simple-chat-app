import 'package:chat_app/chat-screen/components/atoms/message_bubble.dart';
import 'package:chat_app/theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loading_indicator/loading_indicator.dart';

class ListMessage extends StatelessWidget {
  const ListMessage({super.key});

  @override
  Widget build(BuildContext context) {
    final authUser = FirebaseAuth.instance.currentUser!;

    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('chat')
            .orderBy('createdAt', descending: true)
            .snapshots(),
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: LoadingIndicator(
                indicatorType: Indicator.ballSpinFadeLoader,
                colors: [MyAppColor.WHITE],
                strokeWidth: 2,
                backgroundColor: MyAppColor.RED,
              ),
            );
          } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return Center(
              child: Text(
                'No message found',
                style: Theme.of(context).textTheme.labelMedium!,
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text(
                'Error while fetching chat',
                style: Theme.of(context).textTheme.labelMedium!,
              ),
            );
          }

          final loadedMessages = snapshot.data!.docs;
          return ListView.builder(
              padding: EdgeInsets.only(
                bottom: 40.h,
                left: 13.w,
                right: 13.w,
              ),
              reverse: true,
              itemCount: loadedMessages.length,
              itemBuilder: (ctx, index) {
                final chatMessage = loadedMessages[index].data();
                final nextChatMessage = index + 1 < loadedMessages.length
                    ? loadedMessages[index + 1].data()
                    : null;

                final curUserId = chatMessage['userId'];
                final nextUserId =
                    nextChatMessage != null ? nextChatMessage['userId'] : null;
                final isNextUserIdSame = curUserId == nextUserId;

                if (isNextUserIdSame) {
                  return MessageBubble.next(
                    message: chatMessage['text'],
                    isMe: authUser.uid == curUserId,
                  );
                }

                return MessageBubble.first(
                  userImage: chatMessage['userImage'],
                  username: chatMessage['name'],
                  message: chatMessage['text'],
                  isMe: authUser.uid == curUserId,
                );
              });
        });
  }
}
